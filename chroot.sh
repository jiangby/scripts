#!/bin/sh

ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
hwclock --systohc

echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen

echo "LANG=en_US.UTF-8" > /etc/locale.conf
echo a > /etc/hostname
echo -e "127.0.0.1\tarch.localdomain\tarch" >> /etc/hosts
echo "blacklist pcspkr" > /etc/modprobe.d/nobeep.conf
echo "blacklist nouveau
options nouveau modeset=0" >> /etc/modprobe.d/blacklist-nvidia-nouveau.conf
#echo "Section 'InputClass'
#        Identifier 'touchpad'
#        MatchIsTouchpad 'on'
#        Driver 'libinput'
#        Option 'Tapping' 'on'
#        Option 'TappingButtonMap' 'lrm'
#        Option 'NaturalScrolling' 'on'
#        Option 'ScrollMethod' 'twofinger'
#EndSection" > /etc/X11/xorg.conf.d/90-touchpad.conf
#echo "Section 'Device'
#	Identifier	'intel'
#	Driver		'intel'
#	BusId		'PCI:0:2:0'
#EndSection
#
#Section 'Screen'
#	Identifier	'intel'
#	Device		'intel'
#EndSection" > /etc/X11/xorg.conf

mkinitcpio -p linux
passwd

# for bios
#pacman -S --noconfirm grub networkmanager intel-ucode
#grub-install --target=i386-pc /dev/sda

# for efi
pacman -S --noconfirm grub efibootmgr networkmanager intel-ucode
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=archlinux
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager fstrim.timer
#systemctl start NetworkManager fstrim.timer

useradd -m -G wheel -s /bin/bash c
passwd c

cp /install.sh /home/c/install.sh
#curl http://192.168.24.88/scripts/install.sh > /home/c/install.sh
echo "bash install.sh" >> /home/c/.bashrc
echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

exit
umount -R /mnt
reboot
