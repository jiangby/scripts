#!/bin/sh

# dialog sample:
# result=$(dialog --inputbox "test" 10 10 --output-fd 1)
# echo $result

echo -e "\n##### setting up ntp #####\n"
timedatectl set-ntp true

echo -e "\n##### Partition #####\n"
# before run this section, should edit '/dev/sda' and 'disk size'
dd if=/dev/zero of=/dev/sda bs=1k count=2048
sgdisk -Z -o /dev/sda
sgdisk -n 1:0:+512M -t 1:ef00 -c 1:'UEFI' /dev/sda
sgdisk -n 2:0:+1G -c 2:'SWAP' -t 2:8200 /dev/sda
sgdisk -n 3:0:+25G -c 3:'Root' -t 3:8304 /dev/sda
sgdisk -n 4:0:-0 -c 4:'Home' -t 4:8302 /dev/sda

yes | mkfs.ext4 /dev/sda3
mount /dev/sda3 /mnt

yes | mkfs.fat -F32 /dev/sda1
mkdir /mnt/boot
mount /dev/sda1 /mnt/boot

yes | mkfs.ext4 /dev/sda4
mkdir /mnt/home
mount /dev/sda4 /mnt/home

mkswap /dev/sda2
swapon /dev/sda2

echo -e "\n##### fetch faster mirrors #####\n"
pacman -Sy --noconfirm reflector
reflector --verbose --latest 10 --sort rate --save /etc/pacman.d/mirrorlist

#echo -e "Server = http://mirrors.tuna.tsinghua.edu.cn/archlinux/$repo/os/$arch
#$(cat /etc/pacman.d/mirrorlist)" > /etc/pacman.d/mirrorlist

#echo -e "Server = http://mirrors.163.com/archlinux/$repo/os/$arch
#$(cat /etc/pacman.d/mirrorlist)" > /etc/pacman.d/mirrorlist

echo -e "\n##### installing base #####\n"
pacstrap /mnt base base-devel

genfstab -U /mnt >> /mnt/etc/fstab

curl -O https://gitlab.com/jiangby/scripts/raw/master/chroot.sh
curl -O https://gitlab.com/jiangby/scripts/raw/master/install.sh

cp *.sh /mnt/ && arch-chroot /mnt bash chroot.sh && rm /mnt/chroot.sh
#curl http://192.168.24.88/scripts/chroot.sh > /mnt/chroot.sh && arch-chroot /mnt bash chroot.sh && rm /mnt/chroot.sh

umount -R /mnt
reboot
