#!/bin/sh
sudo pacman -S --noconfirm git wget vim zsh
#git clone https://aur.archlinux.org/yay
#cd yay && makepkg -si && cd .. && rm -rf yay

sudo pacman -S --noconfirm xorg-server i3 i3-gaps alsa-utils dmenu xf86-video-intel lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings lxappearance termite ttf-dejavu

#git clone https://gitlab.com/jiangby/st
#cd st
#make clean && sudo make install && cd

sudo pacman -S --noconfirm feh xcompmgr oblogout samba variety arc-gtk-theme chromium shadowsocks-qt5 bash-completion fcitx-im fcitx-configtool fcitx-googlepinyin

sudo systemctl enable lightdm

sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

cd
git clone https://gitlab.com/jiangby/dotfiles
cd dotfiles
sudo cp -rf fackroot/etc/. /etc/
sudo cp -rf fackroot/usr/. /usr/
cp -rf .config/. ~/.config/
cp -rf .local/. ~/.local/
cp .bashrc ~/
cp .vimrc ~/

sed -i "s/bash install.sh//g" ~/.bashrc
rm -f ~/install.sh
reboot
